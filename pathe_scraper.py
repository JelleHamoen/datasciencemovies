import requests
from bs4 import BeautifulSoup
import rdflib
import json


def return_films_frontpage():
    # Legacy function, scrapes front page
    url = 'http://www.pathe.nl/films'
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    film_elements = soup.find_all('div', class_='poster-carousel__item')
    print(film_elements)
    return film_elements


def scrape_films_expected():
    # Scrapes the expected films pathe page and creates the data structure which is the base for the RDF store
    films = []
    url = 'http://www.pathe.nl/films/verwacht'
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    film_elements = soup.find_all('a', class_='poster')
    for film in film_elements:
        elem = {'title': film.find('p', class_='poster__label').text,
                'date': film.find('p', class_='poster__sub').text,
                'img_url': film.find('img')['src']}
        films.append(elem)
    print(films)
    return films


def store_films_as_rdf(films):
    # Takes the films element generated in the "scrape_films_expected" function and
    # creates a rdf store in data/pathe_movies.json
    movies = rdflib.Graph()
    ns = rdflib.Namespace('http://local/')
    movies.bind('movie', ns)
    for i, movie in enumerate(films):
        u = rdflib.term.URIRef('movie{}'.format(i))
        movies.add((u, ns.title, rdflib.Literal(movie.get('title', 'none'))))
        movies.add((u, ns.imgurl, rdflib.Literal(movie.get('img_url', 'none'))))
        movies.add((u, ns.releasedate, rdflib.Literal(movie.get('date', 'none'))))
    movies.serialize(format='json-ld', destination='data/pathe_movies.json')


def load_films_from_rdf(path):
    # Loads the RDF store from path and returns ordered list containing movie ID,title, Image url, and expected date
    with open(path, 'r') as f:
        movies = json.loads(f.read())
    x = sorted(movies, key=lambda k: k['@id'])
    return x


if __name__ == '__main__':
    store_films_as_rdf(scrape_films_expected())
    print(load_films_from_rdf('data/pathe_movies.json'))
