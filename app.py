from flask import Flask, render_template, request, redirect
from dbpedia_manager import query_dbpedia_crew_associations, get_movie_resources
from pathe_scraper import scrape_films_expected, load_films_from_rdf, store_films_as_rdf
from flask_caching import Cache
import re, urllib3

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple', 'FLASK_ENV': 'development'})


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/search')
def search():
    return render_template('search.html')


@app.route('/query')
def db_query():
    if 'r' in request.args:
        # Resource link present, return associations
        url = 'http://dbpedia.org/resource/{}'.format(request.args['r'])
        results = query_dbpedia_crew_associations(url)
        return render_template('match_container.html', result=results,
                               text=[result for result in results["results"]['bindings']])
    if 'q' in request.args:
        # Movie title from input query or scrape, treat both as user input
        # Deal with spaces
        query = re.sub(' ', '_', request.args['q'])
        # remove brackets and any content within
        query = re.sub('\(.*\)', '', query)
        resource = get_movie_resources(query)
        if len(resource) > 1:
            films = [movie['resource']['value'].split('/')[-1] for movie in resource]
            text = [re.sub('_', ' ', movie) for movie in films]
            result = zip(films, text)
            print(result)
            return render_template('select_from_ambiguous.html', results=result)
        if len(resource) == 0 or resource == 404:
            return render_template('search.html',
                                   alert='The movie could not be found, please try again, or pick another film')
        results = query_dbpedia_crew_associations(resource[0]['resource']['value'])
        return render_template('match_container.html', result=results,
                               text=[result for result in results["results"]['bindings']])
    else:
        return render_template('search.html', alert="Please input a query")


@app.route('/expected')
@cache.cached(timeout=300, key_prefix='home_cache')
def list_expected_movies():
    # Get expected films directly from pathe website and return html page
    films = scrape_films_expected()
    return render_template('pathe_container.html', films=films)


@app.route('/rdf')
def load_expected_movies_rdf():
    # Load expected films from RDF store and return htmll page
    films = load_films_from_rdf('data/pathe_movies.json')
    return render_template('pathe_rdf_container.html', films=films)


@app.route('/getmovies')
def scrape_expected_to_rdf():
    films = scrape_films_expected()
    store_films_as_rdf(films)
    return redirect('/rdf')


@app.route('/oldweb')
def load_web():
    # Show off the old web design
    text = [{'name': {'value': 'Film {}'.format(i)}, 'pCount': {'value': i}} for i in range(11)]
    return render_template('old_match_container.html', text=text)


if __name__ == '__main__':
    app.run()
