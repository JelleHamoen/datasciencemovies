document.getElementById('movie_button').onclick = function () {
    let query_text = document.getElementById('movie_input').value;
    const url = new URL(window.location.origin + '/query?q=' + query_text);
    window.location.assign(url);
    return 0;
}

function createHover(elementId) {
    var el = document.getElementById(elementId),
        classList = 'classList' in el;
    for (var i = 0; i < el.children.length; i++) {
        var child = el.children[i];
        if (child.tagName == 'DIV') {
            if (classList) {
                child.classList.add('hover');
            } else {
                child.className += 'hover'
            }
        }
    }
    return true
}

function createOpacity(className) {
    var el = document.getElementsByClassName(className);
    var crewN = document.getElementById('crewN').value
    for (var i = 0; i < el.length; i++) {
        var elem = el[i];
        var match = elem.getAttribute('data-match');
        var matchG = (match / crewN) * 100;
        console.log(matchG)
        elem.style.backgroundColor = '#fff '+matchG+'%';
    }
}

document.onload = function () {
    createOpacity('gallery');
}



