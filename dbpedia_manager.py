from SPARQLWrapper import SPARQLWrapper, JSON, XML
import re

# Init sparql wrapper for the functions that need it
sparql = SPARQLWrapper("http://dbpedia.org/sparql")


def get_movie_resources(user_query):
    # Sanitises user input and looks for the associated DBpedia resource (either EN or NL), returns list of resources
    query = """
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX dbr: <http://dbpedia.org/resource/>
                PREFIX dbp: <http://dbpedia.org/property/>
                SELECT distinct *
                WHERE { 
                ?resource dbp:name ?name .
                ?resource rdf:type dbo:Film
                Filter (langMatches(lang(?name),"EN") || langMatches(lang(?name),"NL")).        
                ?name bif:contains \"""" + re.sub('\W', '', str(user_query)) + """\" }
                LIMIT 20
                """
    try:
        resource = sparql_query(query)['results']['bindings']
    except IndexError:
        return 404
    return resource


def query_dbpedia_crew_associations(movie_resource):
    # Takes movie resource and queries dbpedia for all associated movies.
    # Returns list of movies with amount of common crew, and associated wikipedia link if present
    query = """
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX dbr: <http://dbpedia.org/resource/>
                PREFIX dbp: <http://dbpedia.org/property/>
                SELECT ?name ?page (COUNT(?name) as ?pCount) WHERE{
                {?work ?has ?person .
                ?work rdf:type dbo:Film .
                ?work dbp:name ?name .
                OPTIONAL{?work foaf:isPrimaryTopicOf ?page}
                }
                UNION
                {?work ?of ?person .
                ?work rdf:type dbo:Film .
                ?work dbp:name ?name .
                OPTIONAL{?work foaf:isPrimaryTopicOf ?page}
                }
                {
                SELECT DISTINCT ?person
                WHERE { 
                {?person a foaf:Person; 
                ?of <""" + movie_resource + """>} 
                UNION
                {?person a foaf:Person. 
                <""" + movie_resource + """> ?has ?person}
                }
                }
                }
                GROUP BY ?name ?page
                ORDER BY desc(?pCount)
                LIMIT 25
                """
    # Written with help from https://stackoverflow.com/questions/40901562/find-people-linked-with-person-x-using-sparql-and-dbpedia
    return sparql_query(query)


def sparql_query(query):
    # Wrapper for dbpedia Sparql queries, returns JSON with query results
    sparql_string = query  # room for query editing/handling here
    print(sparql_string)
    sparql.setQuery(sparql_string)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    print(results)
    return results
