# Movie matching and filtering using open data

“The goal of this project is to match upcoming movies with previous produced movies in order to provide the potential customer of cinemas with better insight regarding his or her interest in new upcoming movies”
## About
This is a project for the course "data science" at the university of Twente. Movies are shown and per movie databases are queried, from which movies with the same or similar crew are queried. This application aims at providing an unbiased recommendation web, alongside an insight in which movies have a similar reference frame. This application works on the assumption that the properties of associated movies and the reference frames of their crews are not directly measurable. If movies have overlapping people working on them, the reference frame and feel are supposed to be similar, in a non-trivial and novel manner.

### Functionality (see navbar in the website)
* List expected movies from Pathé
* List expected movies from Pathé, via a pre-made RDF store
* Update the RDF store with the latest info from the Pathé site
* Search associations of a film of your choice

## Getting started
* Create a virtual python environment (not required but recommended)
* 'pip install -r requirements.txt'
* Run app.py
* Navigate to http://127.0.0.1:5000/ on your browser

Made by Jelle Hamoen and Lars Berghuis